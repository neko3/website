---
layout: page
title: Teaching
---

# <i class="far fa-lg fa-file-alt" ></i> ~~Project Supervision~~

I do **not** supervise students anymore. This section was left here as reference.

I am happy to supervise security-related projects. My main interests are embedded devices and contactless payment methods. I welcome projects which include reverse engineering, pentesting, forensics analysis or protocol analysis. I am also interested in improving methods for cybersecurity education, automated exercise/assignment creation for cyber-security, in the style of Capture the Flag challenges.

# <i class="fa fa-lg fa-book" id="teaching"></i> Teaching

<i class="fa fa-check"></i> LM Forensics, Malware and Penetration Testing (2020/21)   
<i class="fa fa-check"></i> LM Security Research Seminar (2015/16 and 2016/17, 2017/18, 2018/19, 2019/20)   
<i class="fa fa-check"></i> LI Introduction to Computer Security (2014/15, 2015/16, 2016/17, 2018/19)   
<i class="fa fa-check"></i> LI Reasoning (2015/16, 2016/17 and 2018/19)   
<i class="fa fa-check"></i> LC Introduction to Artifical Intelligence (2014/15 and 2015/16)   
<i class="fa fa-check"></i> LM Introduction to Artifical Intelligence (2014/15)   
