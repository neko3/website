---
layout: page 
title: Research
---

# <i class="far fa-lg fa-file-alt" ></i> Papers [bib]({{site.baseurl}}/assets/AndreeaInaRadu.bib){:style="float: right;"}

[Practical EMV Relay Protection](https://practical_emv.gitlab.io/assets/practical_emv_rp.pdf){:target="new"}  
_Andreea-Ina Radu, [Tom Chothia](https://www.cs.bham.ac.uk/~tpc/){:target="new"}, [Christopher J.P. Newton](https://www.surrey.ac.uk/people/christopher-j-p-newton"){:target="new"}, [Ioana Boureanu](http://people.itcarlson.com/ioana/){:target="new"} and [Liqun Chen](https://www.surrey.ac.uk/people/liqun-chen){:target="new"}_  
In _43rd IEEE Symposium on Security and Privacy (S&P 2022)_.  
More information, including demo videos and media coverage at [https://practical_emv.gitlab.io](https://practical_emv.gitlab.io){:target="new"}.


[Grey-box Analysis and Fuzzing of Automotive Electronic Components via Control-Flow Graph Extraction]({{site.baseurl}}/publications/greybox_fuzz.pdf){:target="new"}
_Andreea-Ina Radu and [Flavio D. Garcia](https://www.cs.bham.ac.uk/~garciaf/index.html){:target="new"}_  
In _4th Computer Science in Cars Symposium (CSCS 2020)_.  


[Learn-Apply-Reinforce/Share Learning: Hackathons and CTFs as General Pedagogic Tools in Higher Education, and Their Applicability to Distance Learning](https://arxiv.org/pdf/2006.04226.pdf){:target="new"}  
_[Tom Goodman](https://tomg.io/){:target="new"} and Andreea-Ina Radu_  
_ArXiv_ (2020).


[Choose Your Pwn Adventure: Adding Competition and Storytelling to an Introductory Cybersecurity Course]({{site.baseurl}}/publications/pwn.pdf){:target="new"}  
_[Tom Chothia](https://www.cs.bham.ac.uk/~tpc/){:target="new"}, [Chris Novakovic](https://www.cs.bham.ac.uk/~novakovc/){:target="new"}, Sam Holdcroft, Andreea-Ina Radu and [Richard J. Thomas](https://rjthomas.io/){:target="new"}_  
In _Transactions on Edutainment XV_ (2019).


[Jail, Hero or Drug Lord? Turning a Cyber Security Course Into an 11 Week Choose Your Own Adventure Story]({{site.baseurl}}/publications/Jail_Hero_or_Drug_Lord.pdf){:target="new"}  
_[Tom Chothia](https://www.cs.bham.ac.uk/~tpc/){:target="new"}, Sam Holdcroft, Andreea-Ina Radu and [Richard J. Thomas](https://rjthomas.io/){:target="new"}_   
In _USENIX Workshop on Advances in Security Education (ASE 2017)_.   


[LeiA: A Lightweight Authentication Protocol for CAN]({{site.baseurl}}/publications/LeiA.pdf){:target="new"}   
_Andreea-Ina Radu and [Flavio D. Garcia](https://www.cs.bham.ac.uk/~garciaf/index.html){:target="new"}_   
In _21st European Symposium on Research in Computer Security (ESORICS 2016)_.   
Lecture Notes in Computer Science, Vol. 9879, pages 283-300, 2016. Springer Verlag.   


[Organising Monkeys or How to Run a _Hacking_ Club]({{site.baseurl}}/publications/Organising_Monkeys.pdf){:target="new"}   
_Andreea-Ina Radu and [Sam L. Thomas](https://xv.ax/){:target="new"}_   
Vibrant Workshop 2015, The first UK Workshop on Cybersecurity Training &amp; Education    
[Presentation slides]({{site.baseurl}}/publications/Organising_monkeys_pres.pdf){:target="new"}


# <i class="fas fa-lg fa-edit"></i> Academic Refereeing
	
<i class="fa fa-check"></i> IEEE Transactions on Dependable and Secure Computing   
<i class="fa fa-check"></i> IEEE Transactions on Information Forensics and Security   
<i class="fa fa-check"></i> The 15th Conference on Detection of Intrusions and Malware and Vulnerability Assessment (DIMVA'2018)   
<i class="fa fa-check"></i> Springer Journal of Mobile Networks and Applications    
<i class="fa fa-check"></i> IEEE Design &amp; Test Special Issue on Automotive Security   
<i class="fa fa-check"></i> The 11th WISTP International Conference on Information Security Theory and Practice (WISTP'2017)   
<i class="fa fa-check"></i> 2017 USENIX Workshop on Advances in Security Education (ASE '17)    
<i class="fa fa-check"></i> The 15th Embedded Security in Cars Conference - Europe (ESCAR'2016)    
<i class="fa fa-check"></i> The 10th WISTP International Conference on Information Security Theory and Practice (WISTP'2016)   
<i class="fa fa-check"></i> International Symposium on Engineering Secure Software and Systems	(ESSoS'2016)   
<i class="fa fa-check"></i> The 30th ACM/SIGAPP Symposium On Applied Computing (SAC'2015)    
<i class="fa fa-check"></i> The 9th WISTP International Conference on Information Security Theory and Practice (WISTP'2015)   
<i class="fa fa-check"></i> The 18th International Symposium on Research in Attacks, Intrusions and Defenses (RAID'2015)   
