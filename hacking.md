---
layout: page 
title: Hacking
---


<div class="span12">
    <p><strong>Coming soon:</strong> HTB write-ups.</p> 
</div>


<div class="span6">
<p><strong>My write-ups:</strong></p>
<p>CTFS:</p>
<ul>
    <li> tamu2019
        <ul><li><a href="https://afnom.net/writeups/2019/keygenme" target="new">keygenme</a></li></ul>
    </li>
    <li> hxp2018
        <ul><li><a href="https://afnom.net/writeups/2018/hxp-angrme" target="new">angrme</a></li></ul>
    </li>
    <li>Defcamp Quals 2015
        <ul><li><a href="https://afnom.net/writeups/2015/Crypto200" target="new">Crypto 200</a></li></ul>
    </li>
    <li>PoliCTF 2015:
        <ul>
            <li><a href="https://afnom.net/writeups/2015/john-traveller" target="new">John the Traveller</a></li>
            <li><a href="https://afnom.net/writeups/2015/john-referee" target="new">John the Referee</a></li>
        </ul>
    </li>
</ul>
</div>
<div class=" span2">
    <img class="img-responsive" src="{{site.url}}/assets/neko-logo.png" />
</div> 
        
    
    
<div class="row-fluid">
        <div class="span12">
<p>Misc:</p>
    <ul>
    {% for writeup in site.writeups %}
        <li><a href="{{ writeup.url | prepend: site.baseurl  }}" target="new">{{writeup.desc}}</a> </li>
    {% endfor %}
    </ul>
</div>

