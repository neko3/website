---
layout: writeup
title: defusethebomb
date: 2019-03-10
author: neko3
desc: HECC 2019 - defusethebomb RE chall
---

### Intro

One of the reverse engineering challenges at the [HECC 2019](https://hecc.io){:target="_new"} CTF was a bomb defuse [binary]({{ site.baseurl }}/assets/ctf/defusethebomb).

Loading the binary in [IDA Pro](https://www.hex-rays.com/products/ida/){:target="_new"}, we can see the string `It's going to go off!` will be printed, and an ASCII art of a bomb, after which we are asked to enter a password to defuse it. Towards the end of the `main` function, we can see a basic block which tests if the input password is equal to some value. This can lead to either a basic block printing out `Phew! Good job! Here's a flag for you` or one printing out `KABOOM!!`. We obviously want to end on the `Phew` basic block.

Normally, I would look at what happens with the input string and try to figure it out, but as this was a very time constrained competition, I skimmed it and decided to throw [angr](https://angr.io/){:target="_new"} at it, to symbolically execute until it finds the string `Good job` in the output.

{% highlight python %}
import angr

proj = angr.Project("defusethebomb")
simgr = proj.factory.simgr()
simgr.explore(find=lambda s: b"Good job" in s.posix.dumps(1))
s = simgr.found[0]
print(s.posix.dumps(0))
{% endhighlight %}

We get the output: `q8np28xncpts9avzj8`.

Testing it:
{% highlight shell %}
$> ./defusethebomb 
It's going to go off!!
             . . .                         
              \|/                          
            `--+--'                        
              /|\                          
             ' | '                         
               |                           
               |                           
           ,--'#`--.                       
           |#######|                       
        _.-'#######`-._                    
     ,-'###############`-.                 
   ,'#####################`,               
  /#########################\              
 |###########################|             
|#############################|            
|#############################|            
|#############################|            
|#############################|            
 |###########################|             
  \#########################/              
   `.#####################,'               
     `._###############_,'                 
        `--..#####..--'
Disarm password: q8np28xncpts9avzj8

Phew! Good job! Here's a flag for your hard work: y8vx28fvkxba9i
{% endhighlight %}