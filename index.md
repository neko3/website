---
layout: page 
title: Andreea-Ina Radu
tagline: >
            Honorary Senior Researcher Fellow <br />
            <a href="https://www.cs.bham.ac.uk/" target="new">School of Computer Science</a> <br />
            <a href="https://www.birmingham.ac.uk/" target="new">University of Birmingham</a>
---

# <i class="fa-solid fa-terminal "></i> Hello

This is my _personal_ website, which also hosts any research-related content I see fit.
    
<i class="far fa-star"></i> **I am an Honorary Senior Researcher Fellow at the University of Birmingham. Previously, I was a Lecturer and Research Fellow. I have an interest in security and privacy. More specifically, I work on contactless payment systems and embedded devices security, with [Dr. Tom Chothia](https://www.cs.bham.ac.uk/~tpc/){:target="new"}.**

My PhD was on automotive security, and my supervisor was [Prof. Flavio Garcia](https://www.cs.bham.ac.uk/~garciaf/){:target="new"}.

<i class="fa-solid fa-flag fa-lg"></i> I used to organise the Ethical Hacking Club [AFiniteNumberOfMonkeys](https://afnom.net){:target="new"} at UoB -- join the [AFNOM  Discord](https://discord.gg/JpZ2CgX){:target="new"} server if you are a UoB student interested in cybersecurity and CTFs.


# <i class="fas fa-id-card fa-lg fa-inverse"></i> Contact
 
<i class="fa-regular fa-envelope fa-xl" style="color: #00aaaa;"></i> 
research<i class="fa fa-inverse fa-at"></i>inaradu.com  
a.i.radu<i class="fa fa-inverse fa-at"></i>bham.ac.uk  


PGP: 0x[8C5DAF38]({{site.baseurl}}/assets/8C5DAF38_pub.asc){:target="new"}   
PGP fingerprint:   
63DC CAF6 25F1 B274 A44B 3926 99F4 C7E4 8C5D AF38 
	
Join me on:


<a href="https://infosec.exchange/@neko3" target="new">
  <i class="fa-brands fa-mastodon fa-2x"></i>
</a>
<a href="https://twitter.com/Inutza16" target="new">
  <i class="fa-brands fa-twitter-square fa-2x"></i>
</a>
<a href="https://github.com/inutza" target="new">
	<i class="fa-brands fa-github fa-2x"></i>
</a>
<a href="https://gitlab.com/neko3" target="new">
	<i class="fa-brands fa-gitlab fa-2x"></i>
</a>

If you're interested in my professional shenanigans, check out my LinkedIn: 
<a href="https://www.linkedin.com/in/andreea-ina-radu-36b2545b" target="new">
 <i class="fa-brands fa-linkedin fa-2x"></i>
</a>

# <i class="fa fa-coffee fa-lg"></i> Spare time

I like reading <i class="fa-solid fa-book fa-lg"></i> and gaming <i class="fa-brands fa-steam fa-lg"></i>.
